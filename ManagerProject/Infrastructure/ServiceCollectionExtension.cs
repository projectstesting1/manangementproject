﻿using EasyCaching.Core.Configurations;
using ManagerProject.Data;
using ManagerProject.Data.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using static ManagerProject.Features.Common.Constants;

namespace ManagerProject.Infrastructure
{
    public static class ServiceCollectionExtension
    {
        public static AppSettings GetApplicationSettings(
            this IServiceCollection services, IConfiguration configuration)
        {
            var applicationSettingsConfiguration = configuration.GetSection("ApplicationSettings");
            services.Configure<AppSettings>(applicationSettingsConfiguration);
            return applicationSettingsConfiguration.Get<AppSettings>();
        }

        public static IServiceCollection AddDatabase(
            this IServiceCollection services, IConfiguration configuration) => services.AddDbContext<ManagerProjectDbContext>
            (opt => opt.UseSqlServer(configuration.GetDefaultConnectionString()));




        public static IServiceCollection AddIdentity(this IServiceCollection services)
        {
            services
                .AddIdentity<User, IdentityRole>(opt =>
                {
                    opt.Password.RequiredLength = 2;
                    opt.Password.RequireDigit = false;
                    opt.Password.RequireLowercase = false;
                    opt.Password.RequireNonAlphanumeric = false;
                    opt.Password.RequireUppercase = false;
                })
                .AddEntityFrameworkStores<ManagerProjectDbContext>();

            return services;
        }

        public static IServiceCollection AddSignalRWithOptions(this IServiceCollection services)
        {
            services.AddSignalR(opt =>
            {
                opt.EnableDetailedErrors = true;
            });
            return services;
        }


        public static IServiceCollection AddCorsWithOptions(this IServiceCollection services)
        {
            services.AddCors(opt => opt.AddPolicy("CorsPolicy", builder =>
            {
                builder
                    .WithOrigins("http://localhost:3000")
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials()
                    .SetIsOriginAllowed((host) => true);
            }));

            return services;
        }


        public static IServiceCollection AddRedisEasyCaching(this IServiceCollection services)
        {
            services.AddEasyCaching(opt =>

            {
                opt.UseRedis(
                    redisConfig =>
                    {
                        redisConfig.DBConfig.Endpoints.Add(new ServerEndPoint(Redis.Connection, Redis.Port));

                        redisConfig.DBConfig.AllowAdmin = true;
                    },

                    Redis.Channel);

            });
            return services;
        }


        public static IServiceCollection AddJwtAuthentication(
            this IServiceCollection services, AppSettings appSettings
            )
        {
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);


            services
                .AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        RequireExpirationTime = false,
                        ValidateLifetime = true,
                    };
                    x.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var accessToken = context.Request.Query["access_token"];

                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrEmpty(accessToken) &&
                            path.StartsWithSegments("/updatesHub"))
                            {
                                context.Token = accessToken;
                            }
                            return Task.CompletedTask;
                        }
                    };
                });

            return services;
        }
            

    }
}
