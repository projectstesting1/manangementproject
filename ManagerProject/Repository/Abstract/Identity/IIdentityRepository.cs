﻿using ManagerProject.Data.Models;
using ManagerProject.Features;
using ManagerProject.Repository.Models.Identity;

namespace ManagerProject.Repository.Abstract.Identity
{
    public interface IIdentityRepository
    {
        Task<ResultModel<AuthResponseModel>> RegisterUserAsync(string fullName, string userName, string email, string password, string secret);

        Task<ResultModel<AuthResponseModel>> LoginUserAsync(string email, string password, string secret);

        Task LogoutUserAsync(string token);

        Task UpdateUserAsync(string userId, string fullName, string userName, string imageUrl);
        Task<UserListingServerModel> GetAssignmentUser(string id);
        Task<UserListingServerModel> GerUser(string id);
        Task<string[]> GetAllUsersIdsConnectedProjectByIdAsync(int projectId);
        Task<string> AddClaimToUserAsync(string userId, string claimKey, string claimValue, string secret);
        Task AddClaimToUserAsync(string userId, string claimKey, string claimValue);
        Task RemoveClaimFromUserAsync(string userId, string claimType, string claimValue);
        Task<string> GetUsersRefreshedToken(User user, string secret);
    }
}
