﻿namespace ManagerProject.Repository.Abstract.Cache
{
    public interface ICacheRepository
    {

        Task SetAsync(string key, string value);

        Task<string> GetAsync(string key);
    }
}
