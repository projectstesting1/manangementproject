﻿using System.Security.Claims;

namespace ManagerProject.Repository.Abstract.Token
{
    public interface ITokenRepository
    {
        Task<bool> isCurrentActiveToken();

        Task<string> GenerateJwtToken(string userId, string email, string secret, IList<Claim> claims = null);
        Task DeactivateJwtToken(string userId, string token = null);
    }
}
