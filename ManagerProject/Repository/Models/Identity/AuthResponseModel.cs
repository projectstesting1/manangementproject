﻿namespace ManagerProject.Repository.Models.Identity
{
    public class AuthResponseModel
    {
        public string Token { get; set; }

        public UserDetailedModel User { get; set; }
    }
}
