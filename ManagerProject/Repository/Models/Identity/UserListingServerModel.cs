﻿namespace ManagerProject.Repository.Models.Identity
{
    public class UserListingServerModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string ImageUrl { get; set; }
        public string Email { get; set; }

    }
}
