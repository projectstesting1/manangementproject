﻿namespace ManagerProject.Repository.Models.Identity
{
    public class UserDeatailsRepositoryModel
    {
        public string Id { get; set; }

        public string FullName { get; set; }

        public string ImageUrl { get; set; }
    }
}
