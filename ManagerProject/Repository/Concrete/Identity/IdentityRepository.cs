﻿using ManagerProject.Data;
using ManagerProject.Data.Models;
using ManagerProject.Features;
using ManagerProject.Repository.Abstract.Identity;
using ManagerProject.Repository.Abstract.Token;
using ManagerProject.Repository.Models.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using static ManagerProject.Features.Common.Constants;

namespace ManagerProject.Repository.Concrete.Identity
{
    public class IdentityRepository : IIdentityRepository
    {

        private readonly UserManager<User> userManager;
        private readonly ITokenRepository tokenRepository;
        private ManagerProjectDbContext context;

        public IdentityRepository(UserManager<User> userManager, ITokenRepository tokenRepository, ManagerProjectDbContext context)
        {
            this.userManager = userManager;
            this.tokenRepository = tokenRepository;
            this.context = context;
        }

        public async Task<string> AddClaimToUserAsync(string userId, string claimKey, string claimValue, string secret)
        {
            var user = await this.AddNewClaimToUserAsync(userId, claimKey, claimValue);

            await this.tokenRepository.DeactivateJwtToken(user.Id);

            var token = await this.GetUsersRefreshedToken(user, secret);
            return token;
        }

        public async Task AddClaimToUserAsync(string userId, string claimKey, string claimValue)
        {
            var user = await this.AddNewClaimToUserAsync(userId, claimKey, claimValue);
            await this.tokenRepository.DeactivateJwtToken(user.Id);
        }

        public async Task<UserListingServerModel> GerUser(string id)
        {
            return await this.context.Users.Where(x => x.Id == id).Select(x => new UserListingServerModel
            {
                Id = x.Id,
                FullName = x.FullName,
                UserName = x.UserName,
                ImageUrl = x.ImageUrl,
                Email = x.Email,
            })
                 .FirstOrDefaultAsync();
        }

        public async Task<string[]> GetAllUsersIdsConnectedProjectByIdAsync(int projectId)
        {
            throw new NotImplementedException();
        }

        public async Task<UserDeatailsRepositoryModel> GetAssignmentUser(string id)
        {
            return await this.context.Users.Where(x => x.Id == id).Select(x => new UserDeatailsRepositoryModel
            {
                Id = x.Id,
                FullName = x.FullName,
                ImageUrl = x.ImageUrl,
            })
             .FirstOrDefaultAsync();
        }
      

        public async Task<string> GetUsersRefreshedToken(User user, string secret)
        {
            var claims = await this.userManager.GetClaimsAsync(user);
            var token = await this.tokenRepository.GenerateJwtToken(user.Id, user.Email, secret, claims);
            return token;
        }

        public async Task<ResultModel<AuthResponseModel>> LoginUserAsync(string email, string password, string secret)
        {
            var user = await this.userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return new ResultModel<AuthResponseModel>
                {
                    Errors = new string[] { Errors.InvalidLoginAttempt },
                };
            }

            var passwordValid = await this.userManager.CheckPasswordAsync(user, password);
            if (!passwordValid)
            {
                return new ResultModel<AuthResponseModel>
                {
                    Errors = new string[] { Errors.InvalidLoginAttempt },
                };
            }

            var token = await this.GetUsersRefreshedToken(user, secret);
            var userProjects = await this.context.Users.FirstOrDefaultAsync(x => x.Id == user.Id);


            return new ResultModel<AuthResponseModel>
            {
                Result = new AuthResponseModel
                {
                    Token = token,
                    User = new UserDetailedModel
                    {
                        Id = user.Id,
                        FullName = user.FullName,
                        UserName = user.UserName,
                        ImageUrl = user.ImageUrl,
                        Email = user.Email,
                    },
                },
                Success = true,
            };
        }

    

        public async Task LogoutUserAsync(string token)
        {
            await this.tokenRepository.DeactivateJwtToken(null, token);
        }

        public async Task<ResultModel<AuthResponseModel>> RegisterUserAsync(string fullName, string userName, string email, string password, string secret)
        {
            var existingEmail = await this.userManager.FindByEmailAsync(email);

            if(existingEmail != null)
            {
                return new ResultModel<AuthResponseModel>
                {
                    Errors = new string[] { string.Format(Errors.AlreadyRegisteredUser, email) }
                };
            }


            var user = new User()
            {
                UserName = userName,
                FullName = fullName,
                Email = email,
            };

            var registerAttempt = await this.userManager.CreateAsync(user, password);

            if(!registerAttempt.Succeeded)
            {
                return new ResultModel<AuthResponseModel>
                {
                    Errors = registerAttempt.Errors.Select(x => x.Description),
                };
            }

            var token = await this.tokenRepository.GenerateJwtToken(user.Id, email, secret);


            return new ResultModel<AuthResponseModel>
            {
                Result = new AuthResponseModel
                {
                    Token = token,
                    User = new UserDetailedModel
                    {
                        Id = user.Id,
                        FullName = user.FullName,
                        UserName = user.UserName,
                        ImageUrl = user.ImageUrl,
                        Email = user.Email,
                    },
                },
                Success = true,
            };
        }

        public async Task RemoveClaimFromUserAsync(string userId, string claimType, string claimValue)
        {
            var user = await this.userManager.FindByIdAsync(userId);
            var claims = await this.userManager.GetClaimsAsync(user);
            var claim = claims.Where(x => x.Type == claimType && x.Value == claimValue).FirstOrDefault();
            await this.userManager.RemoveClaimAsync(user, claim);

            await this.tokenRepository.DeactivateJwtToken(user.Id);
        }

        public async Task UpdateUserAsync(string userId, string fullName, string userName, string imageUrl)
        {
            var user = await this.userManager.FindByIdAsync(userId);

            if(fullName != string.Empty && fullName != user.FullName)
            {
                user.FullName = fullName;
            }

            if(userName != string.Empty && userName != user.UserName)
            {
                user.UserName = userName;
            }

            if(imageUrl != string.Empty && imageUrl != user.ImageUrl)
            {
                user.ImageUrl = imageUrl;
            }

            this.context.Update(user);
            await this.context.SaveChangesAsync();
        }

        
        

        public Task<ResultModel<AuthResponseModel>> RegisterUserAsync(string fullName, string userName, string eamil, string password, string secret)
        {
            throw new NotImplementedException();
        }

        public Task RemoveClaimFromUserAsync(string userId, string claimType, string claimValue)
        {
            throw new NotImplementedException();
        }

        public Task UpdateUserAsync(string userId, string fullName, string userName, string imageUrl)
        {
            throw new NotImplementedException();
        }

        private async Task<User> AddNewClaimToUserAsync(string userId, string claimKey, string claimValue)
        {
            var user = await this.userManager.FindByIdAsync(userId);
            var claim = new Claim(claimKey, claimValue);
            await this.userManager.AddClaimAsync(user, claim);

            return user;
        }
    }

   
}

