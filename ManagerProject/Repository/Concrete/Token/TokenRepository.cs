﻿using EasyCaching.Core;
using ManagerProject.Repository.Abstract.Cache;
using ManagerProject.Repository.Abstract.Token;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using static ManagerProject.Features.Common.Constants;


namespace ManagerProject.Repository.Concrete.Token
{
    public class TokenRepository : ITokenRepository
    {
        private readonly ICacheRepository cacheRepository;
        private readonly IHttpContextAccessor httpContextAccessor;

        public TokenRepository(ICacheRepository cacheRepository, IHttpContextAccessor httpContextAccessor)
        {
            this.cacheRepository = cacheRepository;
            this.httpContextAccessor = httpContextAccessor;
        }

        public async Task<string> GenerateJwtToken(string userId, string email, string secret, IList<Claim> claims = null)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var identityClaims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, userId),
                new Claim(ClaimTypes.Email, email),
            };

            if(claims != null)
            {
                identityClaims.AddRange(claims);
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(identityClaims),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var encryptedToken = tokenHandler.WriteToken(token);

            await this.cacheRepository.SetAsync(userId, encryptedToken);
            return encryptedToken;
        }

        public async Task DeactivateJwtToken(string userId, string token = null)
        {
            if(token == null)
            {
                token = await this.cacheRepository.GetAsync(userId);
            }
            await this.cacheRepository.SetAsync(this.GetKey(token), "deactivated");
        }

  

        public async Task<bool> isCurrentActiveToken()
        {

            var currentToken = this.GetCurrentToken();
            var isActiveToken = await this.isActiveAsync(currentToken);
            return isActiveToken;
        }

        private async Task<bool> isActiveAsync(string token)
        {
            var tokenKey = this.GetKey(token);
            var result = await this.cacheRepository.GetAsync(tokenKey);
            return result == null;
        }

        private string GetCurrentToken()
        {
            var authorizationHeader = this.httpContextAccessor
                .HttpContext.Request.Headers["authorization"];

            return authorizationHeader == StringValues.Empty ? String.Empty : authorizationHeader.Single().Split(" ").Last();
        }


        private string GetKey(string token) => string.Format(DeactivatedTokenString, token);
    }
}
