﻿using EasyCaching.Core;
using ManagerProject.Repository.Abstract.Cache;
using static ManagerProject.Features.Common.Constants;

namespace ManagerProject.Repository.Concrete.Cache
{
    public class CacheRepository : ICacheRepository
    {
        private readonly IEasyCachingProviderFactory cachingProviderFactory;
        private readonly IEasyCachingProvider cachingProvider;
        public CacheRepository()
        {
            this.cachingProviderFactory = cachingProviderFactory;
            this.cachingProvider = this.cachingProviderFactory.GetCachingProvider(Redis.Channel);
        }
        public async Task<string> GetAsync(string key)
        {
            var cacheValue = await this.cachingProvider.GetAsync<string>(key);
            return cacheValue.Value;
        }

        public async Task SetAsync(string key, string value)
        {
            await this.cachingProvider.SetAsync(key, value, TimeSpan.FromDays(100));
        }
    }
}
