﻿using Microsoft.AspNetCore.Identity;

namespace ManagerProject.Data.Models
{
    public class User : IdentityUser
    {

        public string FullName { get; set; }

        public string ImageUrl { get; set; }


        public DateTime CreatedOn { get; set; }

        public DateTime? ModiefiedOn { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }
    }
}
