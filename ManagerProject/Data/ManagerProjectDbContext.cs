﻿using ManagerProject.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ManagerProject.Data
{
    public class ManagerProjectDbContext : IdentityDbContext<IdentityUser>
    {
        public ManagerProjectDbContext(DbContextOptions<ManagerProjectDbContext> opt)
            : base(opt)
        {
        }

        public DbSet<User> Users { get; set; }



        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // builder.ApplyConfiguration(new UserData);

        }
    }
}
